﻿namespace supermarket_form
{
    partial class third_control
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDeletestaff = new System.Windows.Forms.Button();
            this.btnUpdatestaff = new System.Windows.Forms.Button();
            this.btnCheckstaff = new System.Windows.Forms.Button();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAddStaff);
            this.panel1.Location = new System.Drawing.Point(173, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(138, 83);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnCheckstaff);
            this.panel2.Location = new System.Drawing.Point(465, 95);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(137, 83);
            this.panel2.TabIndex = 21;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnUpdatestaff);
            this.panel3.Location = new System.Drawing.Point(465, 265);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(137, 82);
            this.panel3.TabIndex = 22;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnDeletestaff);
            this.panel4.Location = new System.Drawing.Point(173, 265);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(138, 82);
            this.panel4.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label1.Location = new System.Drawing.Point(198, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 19);
            this.label1.TabIndex = 24;
            this.label1.Text = "Add Staff";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(489, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 19);
            this.label2.TabIndex = 25;
            this.label2.Text = "Check Staff";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(489, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 19);
            this.label3.TabIndex = 26;
            this.label3.Text = "Update Staff";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(198, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 19);
            this.label4.TabIndex = 27;
            this.label4.Text = "Delete Staff";
            // 
            // btnDeletestaff
            // 
            this.btnDeletestaff.BackColor = System.Drawing.Color.Brown;
            this.btnDeletestaff.Image = global::supermarket_form.Properties.Resources.delete_user;
            this.btnDeletestaff.Location = new System.Drawing.Point(1, 0);
            this.btnDeletestaff.Name = "btnDeletestaff";
            this.btnDeletestaff.Size = new System.Drawing.Size(137, 82);
            this.btnDeletestaff.TabIndex = 2;
            this.btnDeletestaff.UseVisualStyleBackColor = false;
            this.btnDeletestaff.Click += new System.EventHandler(this.btnDeletestaff_Click);
            // 
            // btnUpdatestaff
            // 
            this.btnUpdatestaff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnUpdatestaff.Image = global::supermarket_form.Properties.Resources.refresh_button_with_users_inside;
            this.btnUpdatestaff.Location = new System.Drawing.Point(0, 0);
            this.btnUpdatestaff.Name = "btnUpdatestaff";
            this.btnUpdatestaff.Size = new System.Drawing.Size(137, 82);
            this.btnUpdatestaff.TabIndex = 3;
            this.btnUpdatestaff.UseVisualStyleBackColor = false;
            this.btnUpdatestaff.Click += new System.EventHandler(this.btnUpdatestaff_Click);
            // 
            // btnCheckstaff
            // 
            this.btnCheckstaff.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnCheckstaff.Image = global::supermarket_form.Properties.Resources.businessman_in_apresentation_with_a_graphic_on_a_board;
            this.btnCheckstaff.Location = new System.Drawing.Point(0, 0);
            this.btnCheckstaff.Name = "btnCheckstaff";
            this.btnCheckstaff.Size = new System.Drawing.Size(137, 82);
            this.btnCheckstaff.TabIndex = 2;
            this.btnCheckstaff.UseVisualStyleBackColor = false;
            this.btnCheckstaff.Click += new System.EventHandler(this.btnCheckstaff_Click);
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.BackColor = System.Drawing.Color.SeaGreen;
            this.btnAddStaff.Image = global::supermarket_form.Properties.Resources.create_group_button;
            this.btnAddStaff.Location = new System.Drawing.Point(0, 0);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Size = new System.Drawing.Size(137, 82);
            this.btnAddStaff.TabIndex = 1;
            this.btnAddStaff.UseVisualStyleBackColor = false;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // third_control
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "third_control";
            this.Size = new System.Drawing.Size(865, 410);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.Button btnCheckstaff;
        private System.Windows.Forms.Button btnUpdatestaff;
        private System.Windows.Forms.Button btnDeletestaff;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}
